import { Component } from '@angular/core';
import { ApiService } from '../services/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  public product: any;

  constructor(
    public api: ApiService,
  ){
    Swal.fire({
      title: 'Loading...',
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      }
    });
    this.api.product().then((result: any)=>{
      Swal.close();
      this.product = result.data.data;
    }).catch(() => {
      Swal.fire({
        text: "Get Data Failed",
        icon: "error"
      });
    });
  }
  logout()
  {
    this.api.logout().then((result: any)=>{
    });
  }
}

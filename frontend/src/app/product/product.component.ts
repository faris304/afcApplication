import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit{
  public product: any[] | undefined;
  constructor(public api: ApiService)
  {
    Swal.fire({
      title: 'Loading...',
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      }
    });
    var navbar = <HTMLElement>document.getElementById('navbar');
    navbar.classList.remove('navbar-transparent');
    this.api.product().then((result: any)=>{
      Swal.close();
      this.product = result.data.data;
    }).catch(() => {
      Swal.fire({
        text: "Get Data Failed",
        icon: "error"
      });
    });
  }

  ngOnInit(): void {
  }
}

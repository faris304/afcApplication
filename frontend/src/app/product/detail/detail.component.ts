import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit{
  public detail: any;
  public id: any;
  public value = 1;
  constructor(public api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    ){}

  ngOnInit(): void {
    var navbar = <HTMLElement>document.getElementById('navbar');
    navbar.classList.remove('navbar-transparent');
    Swal.fire({
      title: 'Loading...',
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      }
    });
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
      const data = {
        id: this.id
      };
      this.api.detail(data).then((result: any)=>{
        Swal.close();
        this.detail = result.data.data;
      }).catch(() => {
        Swal.fire({
          text: "Get Data Failed",
          icon: "error"
        });
      });
    })
  }

  add(){
    Swal.fire({
      title: 'Loading...',
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      }
    });
    const data = {
      product_id: this.id,
      quantity: this.value
    };
    this.api.add(data).then((result: any) => {
      Swal.fire({
        text: "Add to Cart",
        icon: "success"
      }).then(() => {
        if (result.data.message == "Success") {
          this.router.navigate(['/product']);
        } else {
          Swal.fire({
            text: "Add to Cart Failed",
            icon: "error"
          });
        }
      });
    }).catch(() => {
      Swal.fire({
        text: "Add to Cart Failed",
        icon: "error"
      });
    });
  }

  handleMinus() {
    this.value--;
  }
  handlePlus() {
    this.value++;
  }
}

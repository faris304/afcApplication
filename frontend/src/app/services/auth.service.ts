import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public id: any;
  public name: any;

  constructor(
    private router: Router,
  ) { }

  isAuthenticated(): boolean {
    if (localStorage.getItem('name') === null || localStorage.getItem('name') === '') {
      return false;
    } else {
      return true;
    }
  }

  getId() {
    this.id = localStorage.getItem('id');
    return this.id;
  }
  getName() {
    this.name = localStorage.getItem('name');
    return this.name;
  }
}

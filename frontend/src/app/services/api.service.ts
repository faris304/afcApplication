import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  login(login: any){
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'post',
        url: `${environment.url}/login`,
        data: login
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  logout(){
    var email: string = localStorage.getItem('email') ?? "";
    var token: string = localStorage.getItem('token') ?? "";
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'get',
        url: `${environment.url}/logout`,
        auth: {
          username: email,
          password: token,
        },
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  product(){
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'get',
        url: `${environment.url}/product`,
        headers: {
          'Accept': 'application/json'
        },
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  detail(data: any){
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'post',
        url: `${environment.url}/detail`,
        data: data
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  add(data: any){
    var email: string = localStorage.getItem('email') ?? "";
    var token: string = localStorage.getItem('token') ?? "";
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'post',
        url: `${environment.url}/add`,
        data: data,
        auth: {
          username: email,
          password: token,
        },
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  order(){
    var email: string = localStorage.getItem('email') ?? "";
    var token: string = localStorage.getItem('token') ?? "";
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'get',
        url: `${environment.url}/order`,
        auth: {
          username: email,
          password: token,
        },
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  status(){
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'get',
        url: `${environment.url}/status`,
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  orderAdmin(){
    var email: string = localStorage.getItem('email') ?? "";
    var token: string = localStorage.getItem('token') ?? "";
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'get',
        url: `${environment.url}/orderAdmin`,
        auth: {
          username: email,
          password: token,
        },
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  updateStatus(data: any){
    var email: string = localStorage.getItem('email') ?? "";
    var token: string = localStorage.getItem('token') ?? "";
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'post',
        url: `${environment.url}/updateStatus`,
        data: data,
        auth: {
          username: email,
          password: token,
        },
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  cart(){
    var email: string = localStorage.getItem('email') ?? "";
    var token: string = localStorage.getItem('token') ?? "";
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'get',
        url: `${environment.url}/cart`,
        auth: {
          username: email,
          password: token,
        },
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  confirmOrder(){
    var email: string = localStorage.getItem('email') ?? "";
    var token: string = localStorage.getItem('token') ?? "";
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'post',
        url: `${environment.url}/confirmOrder`,
        auth: {
          username: email,
          password: token,
        },
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }
}

import { Component } from '@angular/core';
import { ApiService } from '../services/api.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  email: any;
  password: any;

  constructor(
    public api: ApiService,
    private router: Router,
  )
  {
    var navbar = <HTMLElement>document.getElementById('navbar');
    navbar.classList.remove('navbar-transparent');
  }

  onSubmit() {
    Swal.fire({
      title: 'Loading...',
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      }
    });
    const data = {
      email: this.email,
      password: this.password
    };
    this.api.login(data).then((result: any)=>{
      Swal.close();
      if (result.data.data == 'Unauthorised')
      {
        Swal.fire({
          title: 'Unauthorised',
          icon: 'error',
          confirmButtonText: 'OK',
          reverseButtons: true
        });

      } else {
        localStorage.setItem('email', result.data.data.email);
        localStorage.setItem('name', result.data.data.name);
        localStorage.setItem('token', result.data.data.token);
        localStorage.setItem('admin', result.data.data.admin);
        window.location.href = "http://localhost:4200/";
      }
    }).catch(() => {
      Swal.fire({
        text: "Login Failed",
        icon: "error"
      });
    });;
  }
}

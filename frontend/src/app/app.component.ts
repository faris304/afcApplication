import { Component, ElementRef, Inject, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT, Location } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'frontend';

  constructor(
    private router: Router,
    public location: Location
  ) {}
  ngOnInit(): void {
    var navbar = <HTMLElement>document.getElementById('navbar');
    document.addEventListener('scroll', (e)=>{
      const scrollY = (e.target as Element).scrollTop;
      if (this.router.url == '/')
      {
        if (scrollY > 150) {
          // add logic
          navbar.classList.remove('navbar-transparent');
        } else {
          // remove logic
          navbar.classList.add('navbar-transparent');
        }
      }
  }, true);
  }
}

import { Component, OnInit, ElementRef } from '@angular/core';
import { Location} from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public name: any;
  public login: any;
  public isadmin = false;
  public cart: any;
  public html: any;

    constructor(
      public location: Location,
      private element : ElementRef,
      public auth: AuthService,
      public api: ApiService,
    ) {
      const admin = parseInt(localStorage.getItem('admin') ?? '');
      if (admin > 0) {
        this.isadmin = true;
      }
    }

    logout(){
      Swal.fire({
        title: 'Loading...',
        timerProgressBar: true,
        didOpen: () => {
          Swal.showLoading();
        }
      });
      this.api.logout().then(() => {
        localStorage.setItem('email', '');
        localStorage.setItem('name', '');
        localStorage.setItem('token', '');
        localStorage.setItem('admin', '');
        Swal.fire({
          text: "Logout Successful",
          icon: "success"
        }).then(() => {
          window.location.href = "http://localhost:4200/";
        });
      }).catch(() => {
        Swal.fire({
          text: "Logout Failed",
          icon: "error"
        });
      });
    }

    ngOnInit() {
      this.isLogin();
    }
    home(){
      window.location.href = "http://localhost:4200/";
    }
    isLogin() {
      this.login = this.auth.isAuthenticated();
      if (this.login)
      {
        this.name = this.auth.getName();
      }
    }

    showCartPopup() {
      Swal.fire({
        title: 'Loading...',
        timerProgressBar: true,
        didOpen: () => {
          Swal.showLoading();
        }
      });
      // Use SweetAlert to show a pop-up here
      this.api.cart().then(($result: any) => {
        this.cart = $result.data.data;
        this.html = '<table id="table" border=0 style="width: 100%"><thead><tr><th>Product</th><th>Quantity</th></tr></thead><tbody>'
        for (let index = 0; index < this.cart.length; index++) {
          var row = '<tr><td>'+ this.cart[index].product.name+ '</td><td>' + this.cart[index].quantity + '</td></tr>';
          this.html += row;
        }
        this.html += '</tbody></table>';
        Swal.fire({
          title: 'Cart',
          confirmButtonText: 'Confirm Order',
          cancelButtonText: 'Okay',
          showCancelButton: true,
          denyButtonText: 'Okay',
          html: this.html,
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire({
              title: 'Loading...',
              timerProgressBar: true,
              didOpen: () => {
                Swal.showLoading();
              }
            });
            this.api.confirmOrder().then(() => {
              Swal.fire({
                text: "Order Confirmed",
                icon: "success"
              });
            }).catch(() => {
              Swal.fire({
                text: "Confirmation Failed",
                icon: "error"
              });
            });
          }
        });
      }).catch(() => {
        Swal.fire({
          text: "Get Data Failed",
          icon: "error"
        });
      });

  }
}

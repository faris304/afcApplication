import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit{
  public data: any;
  public isadmin = false;
  public status: any;

  constructor(
    public api: ApiService,
    private router: Router,
    ){
    const admin = parseInt(localStorage.getItem('admin') ?? '');
    this.api.status().then((response: any) => {
      this.status = response.data.data;
    }).catch(() => {
      Swal.fire({
        text: "Get Data Failed",
        icon: "error"
      });
    });
    if (admin > 0) {
      this.isadmin = true;
    }
    Swal.fire({
      title: 'Loading...',
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      }
    });
    var navbar = <HTMLElement>document.getElementById('navbar');
    navbar.classList.remove('navbar-transparent');
    if (!this.isadmin) {
      this.api.order().then((result: any)=>{
        Swal.close();
        this.data = result.data.data;
      }).catch(() => {
        Swal.fire({
          text: "Get Data Failed",
          icon: "error"
        });
      });
    } else {
      this.api.orderAdmin().then((result: any)=>{
        Swal.close();
        this.data = result.data.data;
      }).catch(() => {
        Swal.fire({
          text: "Get Data Failed",
          icon: "error"
        });
      });
    }
  }

  updateStatus(id: any, statusid: any){
    Swal.fire({
      title: 'Loading...',
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      }
    });
    const data = {
      id: id,
      statusid: statusid
    };
    this.api.updateStatus(data).then((result: any)=>{
      Swal.close();
      window.location.reload();
    }).catch(() => {
      Swal.fire({
        text: "Update Failed",
        icon: "error"
      });
    });
  }

  ngOnInit(): void {
  }
}

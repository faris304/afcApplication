<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('status')->delete();
        \DB::table('status')->insert(array (
            (
                ['id' => 1 ,'status' => 'Cart']
            ),
            (
                ['id' => 2,'status' => 'Order Confirmed']
            ),
            (
                ['id' => 3,'status' => 'Processing']
            ),
            (
                ['id' => 4 ,'status' => 'Shipped']
            ),
            (
                ['id' => 5 ,'status' => 'Out for Delivery']
            ),
            (
                ['id' => 6 ,'status' => 'Cancelled']
            )
        ));

        \DB::table('product')->delete();
        \DB::table('product')->insert(array (
            (
                ['id' => 1 ,'url' => 'shirt.jpg' ,'name' => 'AFC Shirt' ,'status' => 1 ,'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at leo volutpat, mollis mauris vel, cursus odio. Ut sapien tellus, convallis et nibh et, facilisis lacinia mi. Proin tristique ac ligula posuere tincidunt. Integer gravida volutpat urna, at tincidunt orci. Suspendisse euismod et diam sed consequat. Curabitur tincidunt faucibus justo, vitae porta tellus eleifend ac. Vestibulum at dui vel tellus porta sagittis nec ac augue. Sed enim nunc, pellentesque ac elit dignissim, aliquam egestas libero. Vivamus rhoncus risus lorem, sit amet ultrices elit volutpat sed. Aliquam sit amet semper nunc, vestibulum finibus nisi. Aliquam nec tincidunt lacus, vitae condimentum est. Sed viverra in sem vitae pretium. Donec auctor orci a elit tincidunt, non hendrerit eros hendrerit. Nunc tincidunt convallis quam, sed rutrum erat porta dapibus.']
            ),
            (
                ['id' => 2 ,'url' => 'afccap.jpg' ,'name' => 'AFC Cap' ,'status' => 1 ,'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed ligula quam. Sed et mollis.']
            ),
            (
                ['id' => 3 ,'url' => 'afcBall.jpg' ,'name' => 'AFC Ball' ,'status' => 1 ,'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at leo volutpat, mollis mauris vel, cursus odio. Ut sapien tellus, convallis et nibh et, facilisis lacinia mi. Proin tristique ac ligula posuere tincidunt. Integer gravida volutpat urna, at tincidunt orci. Suspendisse euismod et diam sed consequat. Curabitur tincidunt faucibus justo, vitae porta tellus eleifend ac. Vestibulum at dui vel tellus porta sagittis nec ac augue. Sed enim nunc, pellentesque ac elit dignissim, aliquam egestas libero. Vivamus rhoncus risus lorem, sit amet ultrices elit volutpat sed. Aliquam sit amet semper nunc, vestibulum finibus nisi. Aliquam nec tincidunt lacus, vitae condimentum est. Sed viverra in sem vitae pretium. Donec auctor orci a elit tincidunt, non hendrerit eros hendrerit. Nunc tincidunt convallis quam, sed rutrum erat porta dapibus.']
            ),
        ));

        \DB::table('users')->delete();
        \DB::table('users')->insert(array (
            (
                ['id' => 1 ,'name' => 'admin' ,'email' => 'admin@admin.com' ,'password' => bcrypt('admin'), 'isAdmin' => '1']
            ),
            (
                ['id' => 2 ,'name' => 'faris' ,'email' => 'faris_304@yahoo.com.my' ,'password' => bcrypt('faris'), 'isAdmin' => '0']
            ),
        ));
    }
}

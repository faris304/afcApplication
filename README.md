## Getting Started

*Clone this repository locally:*

``` bash
git clone https://gitlab.com/faris304/afcApplication.git
```

*Rename env file from .env.example to .env*

*composer install*

``` bash
cd afcApplication
composer install
```
*Up using docker*

``` bash
docker-compose up
```
Open new terminal at project directory, run code below

*Migration And Seeder*

``` bash
php artisan migrate:refresh --seed
```

if you have problem with db host during migration, change 'DB_HOST=database' to 'DB_HOST=127.0.0.1' at env file. 
``` bash
# DB_HOST=database
DB_HOST=127.0.0.1

```
After complete migrate, change it back to DB_HOST=database
``` bash
DB_HOST=database
#DB_HOST=127.0.0.1

```
Change directory to frontend
``` bash
cd frontend
```
Install angular dependencies
``` bash
npm install
```
Startup web application
``` bash
npm start
``` 
``` bash
http://localhost:4200/
``` 

User from Seeder (Admin user for changing order status at "Track Your Order" page)
``` bash
Email = faris_304@yahoo.com.my  Password = faris
``` 
``` bash
Email = admin@admin.com  Password = admin
``` 
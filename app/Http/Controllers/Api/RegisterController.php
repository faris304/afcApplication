<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Helper;
use Illuminate\Support\Str;

class RegisterController extends BaseController
{
    /**
    * Register api
    *
    * @return \Illuminate\Http\Response
    */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }


        $input = $request->all();
        $find = User::where('email', $input['email'])->get();

        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('AfcApp')->accessToken;
        $success['name'] =  $user->name;
        $success['admin'] =  $user->isAdmin;

        return $this->sendResponse($find, 'User register successfully.');
    }

    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $user->api_token = Str::random(80);
            $success['token'] =  $user->api_token;
            $success['name'] =  $user->name;
            $success['email'] =  $user->email;
            $success['admin'] =  $user->isAdmin;
            $user->save();

            return $this->sendResponse($success, 'User login successfully.');
        }
        else{
            return $this->sendResponse('Unauthorised', 'User login successfully');
        }
    }

    public function logout(Request $request) {
        $user = Auth::user();
        $user->api_token = '';
        $user->save();

        return $this->sendResponse('Logout', 'Logout successfully.');
      }
}

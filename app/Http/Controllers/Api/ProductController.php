<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\Status;
use Illuminate\Support\Facades\Auth;

class ProductController extends BaseController
{
    public function product(Request $request) {
        $product = Product::get();
        return $this->sendResponse($product, "Success");
    }

    public function detail(Request $request) {
        $id = $request->all();
        $id = $id['id'];
        $product = Product::where('id', $id)->get();
        return $this->sendResponse($product, "Success");
    }

    public function add(Request $request) {
        $order = $request->all();
        $user = Auth::user();
        $addorder = Order::create([
            'product_id' => intval($order['product_id']),
            'quantity' => $order['quantity'],
            'user_id' => $user->id,
            'status_id' => 1
        ]);

        return $this->sendResponse($addorder, "Success");
    }

    public function order(Request $request) {
        $user = Auth::user();
        $product = Order::with("product","status")->where('status_id','>','1')->where('user_id', $user->id)->orderBy('id', 'ASC')->get();
        return $this->sendResponse($product, "Success");
    }

    public function orderAdmin(Request $request) {
        $product = Order::with("product","status")->orderBy('id', 'ASC')->get();
        return $this->sendResponse($product, "Success");
    }

    public function status(Request $request) {
        $status = Status::orderBy('id', 'ASC')->get();
        return $this->sendResponse($status, "Success");
    }

    public function updateStatus(Request $request) {
        $order = $request->all();
        $updOrder = Order::find(intval($order['id']));
        $updOrder->status_id = intval($order['statusid']);
        $updOrder->save();

        return $this->sendResponse($updOrder, "Success");
    }

    public function cart(Request $request) {
        $user = Auth::user();
        $cart = Order::with("product","status")->where('status_id','=','1')->where('user_id', $user->id)->orderBy('id', 'ASC')->get();
        return $this->sendResponse($cart, "Success");
    }

    public function confirmOrder(Request $request) {
        $user = Auth::user();
        $updOrder = Order::where('user_id', '=', $user->id)->where('status_id', '=', 1)->update(['status_id' => 2]);
        return $this->sendResponse($updOrder, "Success");
    }
}

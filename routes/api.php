<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\RegisterController;
use App\Http\Controllers\Api\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
Route::get('logout', [RegisterController::class, 'logout'])->name("auth.logout");
Route::post('add', [ProductController::class, 'add'])->name("add");
Route::get('order', [ProductController::class, 'order'])->name("order");
Route::get('cart', [ProductController::class, 'cart'])->name("cart");
Route::get('orderAdmin', [ProductController::class, 'orderAdmin'])->name("orderAdmin");
Route::post('updateStatus', [ProductController::class, 'updateStatus'])->name("updateStatus");
Route::post('confirmOrder', [ProductController::class, 'confirmOrder'])->name("confirmOrder");
});
Route::get('product', [ProductController::class, 'product'])->name("product");
Route::post("register", [RegisterController::class, 'register'])->name("auth.register");
Route::post('login', [RegisterController::class, 'login'])->name("auth.login");
Route::post('detail', [ProductController::class, 'detail'])->name("detail");
Route::get('status', [ProductController::class, 'status'])->name("status");
